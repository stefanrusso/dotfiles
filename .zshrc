# Created by newuser for 4.8
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/stef/.zshrc'

# export TERMINAL=/usr/bin/kitty
export EDITOR=nvim
# export MOZ_ENABLE_WAYLAND=1

autoload -Uz compinit
compinit
# End of lines added by compinstall

eval "$(starship init zsh)"

alias ls='exa -g'
#export PATH="${PATH}:${HOME}/.config/emacs/bin/"
export EMACSDIR="${HOME}/.config/emacs/"

#Neovim true color support
# export NVIM_TUI_ENABLE_TRUE_COLOR=1
#Neovim cursor shape support
#export NVIM_TUI_ENABLE_CURSOR_SHAPE=1
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent
