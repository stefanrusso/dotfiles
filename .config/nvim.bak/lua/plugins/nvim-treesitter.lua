return {
	"nvim-treesitter/nvim-treesitter",
	enabled = true,
	config = function()
		require('nvim-treesitter.configs').setup {
			  ensure_installed = { 
				  "c",
				  "cpp",
				  "make",
				  "cmake",
				  "lua", 
				  "vim", 
				  "vimdoc", 
				  "query", 
				  "rust",
				  "toml",
				  "yaml",
				  "python", 
				  "comment", 
				  "markdown", 
				  "markdown_inline"},
			  sync_install = false,
			  auto_install = true,
			  highlight = {
				  enable = true,
				  additional_vim_regex_highlighting = false,
			  },
		}
	end,
}
