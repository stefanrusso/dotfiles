return {
	"sainnhe/gruvbox-material",
	enabled = true,
	config = function()
		vim.o.background = "dark"

		vim.g.gruvbox_material_background = "hard"
		vim.g.gruvbox_material_foreground = "original"
		vim.g.gruvbox_material_better_performance = 1

		vim.cmd([[colorscheme gruvbox-material]])
	end,
}
