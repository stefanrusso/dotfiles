export EDITOR=nvim

if [[ $XDG_SESSION_TYPE == "wayland" ]]; then
	export _JAVA_AWT_NONREPARENTING=1
	export MOZ_ENABLE_WAYLAND=1
fi

eval "$(starship init bash)"

if command -v "exa" &>/dev/null; then
	alias ls='exa'
fi

export PATH="${PATH}:${HOME}/.config/emacs/bin/"
export NVIM_TUI_ENABLE_TRUE_COLOR=0
export DOOMDIR="${HOME}/.config/doom"
export VDPAU_DRIVER=radeonsi
. "$HOME/.cargo/env"

export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK="/run/user/$UID/gnupg/S.gpg-agent.ssh"
gpg-connect-agent updatestartuptty /bye >/dev/null
